import React, { Component } from 'react'
import './App.css'
import PostList from './Oefen/PostList'
import PostForm from './Oefen/PostForm'

export class App extends Component {
    render() {
        return (
            <div className="App">
                <PostForm />
                {/* <PostList /> */}
            </div>
        )
    }
}

export default App
