import React from 'react'

const Hello = () => {
   /* return (
        <div className='dummyClass'>
            <h1>Hello Vishwas</h1>
            </div>
        )*/
        //Hieronder worden 2 elementen aangemaakt:
        // 1ste element div met een id hello en className 'Dummyclass'
        // 2de element H1 zonder properties en als tekst 'Hello vishwas' 
    return React.createElement('div', { id: 'hello', className: 'Dummyclass' }, React.createElement('h1', null, 'Hello Vishwas')
    )
}

export default Hello