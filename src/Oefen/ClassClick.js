import React, { Component } from 'react'

class ClassClick extends Component {
    //In het clickHandler event wordt de melding 'Clicked the button' weergegeven in de console log.
    clickHandler(){
        console.log('Clicked the button')
    }

    render(){
        return (
            <div>   
                {/* Wanneer er op de button is geklikt, wordt het event 'clickHandler' opgeroepen. */}
                <button onClick={this.clickHandler}>Click me</button>
            </div>
        )
    }
}

export default ClassClick