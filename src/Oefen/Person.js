import React from 'react'

//De functie 'Person' wordt aangemaakt met de props person en key van de pagina NameList.js.
function Person({person, key}) {
    return (
        <div>
         { /*De gegevens van de objecten worden hieronder uitgelezen. */ }
         <h2>
          {key}  I am {person.name}. I am {person.age} years old. I know {person.skill}
        </h2>    
        </div>
    )
}

export default Person
