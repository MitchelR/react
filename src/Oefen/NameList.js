import React from 'react'
//De pagina Person.js wordt opgeroepen.
import Person from './Person'

function NameList() {
//De objecten worden in de variabele persons gestopt.
const names = ['Bruce', 'Clark', 'Diana', 'Bruce']
const persons = [
    {
        id: 1,
        name: 'Bruce',
        age: 30,
        skill: 'React'
    },
    {
        id: 2,
        name: 'Clark',
        age: 25,
        skill: 'Angular'
    },
    {
        id: 3,
        name: 'Diana',
        age: 28,
        skill: 'Vue'
    }
]

//In de variabele 'personList' worden de gegegevens gestopt van de objecten(personen).
const nameList = names.map((name, index) => <h2 key={index}>{index} {name} </h2>)
    return <div> {nameList} </div>
}

export default NameList
