import React, {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

//De function 'shop' wordt aangemaakt
function Shop() {

    //Met useEffect de fetchItems runnen.  
    useEffect(() => {
        fetchItems();
    //Hieronder worden de lege haakje aangegeven om de functie te runnen nadat het "gemount" is;
    },[]);

    //Items in een state stoppen  
    const [items, setItems] = useState([]);

    //Variabele fetchItems wordt aangemaakt met daarbij async, omdat het van een database komt.
    const fetchItems = async () => {
        //Variabele data en daarin wordt de array van de onderstaande link gestopt.
        const data = await fetch(
            'https://jsonplaceholder.typicode.com/users'
            )      
            //Data converteren naar json
            const items = await data.json()
            setItems(items)
    }
    //Hieronder worden de objecten uitgelezen met de daarbijhorende link.
    return (
        <div>
            {items.map(
                item => (
                <h1 key={item.id}>
                    <Link to={`/shop/${item.id}`}>{item.name}</Link>
                </h1>
                )
            )}
        </div>
    )
}

export default Shop
