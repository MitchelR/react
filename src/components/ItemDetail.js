import React, {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

//De function 'ItemDetail' wordt aangemaakt en daarmee wordt match meegegeven(Met match krijg je toegang tot meer eigenschappen). 
function ItemDetail({match}) {

    //Met useEffect de fetchItems runnen.
    useEffect(() => {
        fetchItem()
    //Hieronder worden de lege haakje aangegeven om de functie te runnen nadat het "gemount" is;
    }, [])

    //Items in een state stoppen
    const [item, setItem] = useState({});

    //Variabele fetchItems wordt aangemaakt met daarbij async, omdat het van een database komt.
    const fetchItem = async () => {
        //Variabele data en daarin wordt de array van de onderstaande link gestopt.
        const fetchItem = await fetch('https://jsonplaceholder.typicode.com/users?id=${match.params.id}')
    //Data converteren naar json
    const item = await fetchItem.json()
    setItem(item)
    console.log(item)
}

    return (
        <div>
            <h1>Item</h1>
        </div>
    )
}

export default ItemDetail
