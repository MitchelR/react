import React from 'react'
import { Link } from 'react-router-dom'

function Nav() {

    //De variabele navStyle wordt aangemaakt.
    const navStyle = {
        color: 'white'
    }

    return (
        <nav>
            <h3>Logo</h3>
            <ul className='nav-Links'>
                    { /* Menu items en de links met daarbij de opmaak van de variabele navStyle*/ }
                    <Link style={navStyle} to='/about'>
                        <li>About</li>
                    </Link>
                    <Link style={navStyle} to='/shop'>
                        <li>Shop</li>
                    </Link>    
            </ul>
        </nav>
    )
}

export default Nav
