import React from 'react';
import './App.css';
import Nav from './components/Nav'
import About from './components/About'
import Shop from './components/Shop'
import ItemDetail from './components/ItemDetail'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import NameList from './Oefen/NameList';
import Stylesheet from './Oefen/Stylesheet';
import Inline from './Oefen//Inline';
import './Oefen/appStyles.css'
import styles from './Oefen/appStyles.module.css'


//De function 'App' wordt aangemaakt.
function App() {
  return (
      <Router>
      <div className='App'>
        <Inline />
        {/*<Stylesheet primary='true' />
        Hier wordt de Nav function opgeroepen */}
        <Nav />
  { /*Switch wordt gebruikt om ervoor te zorgen dat alleen de component wordt geladen van de opgeroepen pagina. */ }
        <Switch>
        { /* Hieronder worden de componenten van de desbetreffende pagina opgeroepen */ }
        <Route path='/' exact component={Home} />
        <Route path='/about' component={About} />
        <Route path='/shop' exact component={Shop} />
        <Route path='/shop/:id' component={ItemDetail} />
        </Switch>
        <NameList />
      </div>
      </Router>
  );
}

/*Wanneer de homepagina wordt geladen, wordt deze div geladen. */
const Home = () => (
  <div>
    <h1>Home Page</h1>
  </div>
)

export default App;
